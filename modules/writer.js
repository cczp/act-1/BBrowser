/*
 * BBrowser bookmarks and history modules.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

app.bookmarks = {
	//Secondary variables
		element: undefined,
		itemPattern:
			"<a class=\"button flat\" href=\"$href\" target=\"curWebView\"" +
			"onclick=\"app.shell.dialogs.hide(\'Bookmarks\')\"" +
			"oncontextmenu=\"app.bookmarks.removeAt($number)\">$data</a>",
	
	//Methods
		get: function() {
			this.element.innerHTML = "";
			for (var i = 0; i < app.settings.list.other.bookmarks.length; i++) {
				this.element.innerHTML = this.itemPattern
					.replace("$href", app.settings.list.other.bookmarks[i].href)
					.replace("$number", i)
					.replace("$data",
						"<span class=\"title\">" + app.settings.list.other.bookmarks[i].title + "</span><br>" +
						"<span class=\"href\">" + app.settings.list.other.bookmarks[i].href + "</span><br>" +
						"<span class=\"date\">" + app.settings.list.other.bookmarks[i].date + "</span>"
					)
					
					+ this.element.innerHTML;
			}
		},
		
		add: function() {
			var date = new Date();
			
			app.settings.list.other.bookmarks.push({
				title: app.webView.element.contentWindow.document.title,
				href: app.webView.element.contentWindow.location.toString(),
				date: 
					("0" + date.getHours()).slice(-2) + ":" +
					("0" + date.getMinutes()).slice(-2) + ":" +
					("0" + date.getSeconds()).slice(-2) + ", " +
					("0" + date.getDate()).slice(-2) + "." +
					("0" + date.getMonth()).slice(-2) + "." +
					date.getFullYear()
			});
			app.settings.config.write("other", "bookmarks");
			
			this.get();
		},
		
		addCustom: function() {
			var 
				adress, name,
				date = new Date();
			
			adress = prompt("Site adress:", "");
			if (adress != null) {
				name = prompt("Site name:", "");
				if (name != null) {
					app.settings.list.other.bookmarks.push({
						title: name,
						href: app.settings.parse("url", adress),
						date: 
							("0" + date.getHours()).slice(-2) + ":" +
							("0" + date.getMinutes()).slice(-2) + ":" +
							("0" + date.getSeconds()).slice(-2) + ", " +
							("0" + date.getDate()).slice(-2) + "." +
							("0" + date.getMonth()).slice(-2) + "." +
							date.getFullYear()
					});
					app.settings.config.write("other", "bookmarks");
					
					this.get();
				}
			}
		},
		
		removeAt: function(number) {
			app.settings.list.other.bookmarks.splice(number, 1);
			app.settings.config.write("other", "bookmarks");
			
			this.get();
		},
		
		clear: function() {
			app.settings.list.other.bookmarks = [];
			app.settings.config.write("other", "bookmarks");
			
			this.get();
		}
};
		
app.history = {
	//Secondary variables
		element: undefined,
		itemPattern:
			"<a class=\"button flat\" href=\"$href\" target=\"curWebView\"" +
			"onclick=\"app.shell.dialogs.hide(\'History\')\"" +
			"oncontextmenu=\"app.history.removeAt($number)\">$data</a>",
			
		isIncognitoModeActivated: false,
	
	//Methods
		get: function() {
			this.element.innerHTML = "";
			for (var i = 0; i < app.settings.list.other.history.length; i++) {
				this.element.innerHTML = this.itemPattern
					.replace("$href", app.settings.list.other.history[i].href)
					.replace("$number", i)
					.replace("$data",
						"<span class=\"title\">" + app.settings.list.other.history[i].title + "</span><br>" +
						"<span class=\"href\">" + app.settings.list.other.history[i].href + "</span><br>" +
						"<span class=\"date\">" + app.settings.list.other.history[i].date + "</span>"
					)
					
					+ this.element.innerHTML;
			}
		},
		
		add: function() {
			var date = new Date();
			
			app.settings.list.other.history.push({
				title: app.webView.element.contentWindow.document.title,
				href: app.webView.element.contentWindow.location.toString(),
				date: 
					("0" + date.getHours()).slice(-2) + ":" +
					("0" + date.getMinutes()).slice(-2) + ":" +
					("0" + date.getSeconds()).slice(-2) + ", " +
					("0" + date.getDate()).slice(-2) + "." +
					("0" + date.getMonth()).slice(-2) + "." +
					date.getFullYear()
			});
			app.settings.config.write("other", "history");
			
			this.get();
		},
		
		removeAt: function(number) {
			app.settings.list.other.history.splice(number, 1);
			app.settings.config.write("other", "history");
			
			this.get();
		},
		
		clear: function() {
			app.settings.list.other.history = [];
			app.settings.config.write("other", "history");
			
			this.get();
		},
		
		
		switchIncognitoMode: function() {
			if (this.isIncognitoModeActivated) {
				this.isIncognitoModeActivated = false;
				
				alert("Incognito mode is deactivated");
			} else {
				this.isIncognitoModeActivated = true;
				
				alert("Incognito mode is activated");
			}
		}
};
